#! /usr/bin/env python3
"""
Copyright (C)2018 iespc.free.fr
Ce projet est un logiciel libre : vous pouvez le redistribuer, le modifier
selon les terme de la GPL (GNU Public License) dans les termes de la
Free Software Foundation concernant la version 3 ou plus de la dite licence.
Ce programme est fait avec l'espoir qu'il sera utile mais SANS AUCUNE GARANTIE.
Lisez la licence pour plus de détails. <http://www.gnu.org/licenses/>.
"""
import os
from datetime import datetime, date, timedelta
from math import sqrt
import locale
import numpy as np
from astropy.coordinates import get_body_barycentric
from astropy.time import Time
from PIL import Image, ImageDraw, ImageFont

locale.setlocale(locale.LC_ALL, "")
if not os.path.exists("./media"):
    os.makedirs("./media")

# Paramètres
# -----------------------------------------------------------------------------
PLANETS = ['mercury', 'venus', 'earth', 'mars'] # planètes à représenter
IMG_L = 1024 # Largeur de l'image
IMG_H = 768 # Hauteur de l'image
IMG_P = 1 # Pourcentage de remplissage de l'image en hauteur
TAILLE_REF = 48 # Taille de référence, en pixels, pour la Terre
DEBUT = date(2018, 1, 28)  # date de début
FIN = date(2019, 1, 28)  # date de fin
INTERVALLE = 15 # jours
IMAGE_FOND = 'milky-way.png' # Image de fond, dans le répertoire 'img'
AUTEUR = 'iespc.free.fr' # Auteur pour la licence
POLICE = 'DejaVuSans' # Police de caractères
TAILLE_POLICE = 22 # Taille de police par défaut (dates)
TAILLE_POLICE_AUTEUR = 14 # Taille de police pour l'AUTEUR
TAILLE_POLICE_ETALON = 20 # Taille de police pour l'étalon
TAILLE_POLICE_NOMS = 18 # Taille de police pour le nom des planètes
COULEUR_AUTEUR = 'white' # Couleur de police pour l'AUTEUR
COULEUR_DATE = (255, 255, 0) # Couleur de police pour la date
COULEUR_ETALON = (0, 255, 0) # Couleur pour l'étalon
COULEUR_NOMS = (255, 255, 0) # Couleur de police pour le nom des planètes
MARGE = 20 # Marge en pixel
#Options
LICENCE_TYPE = 'CC-BY-SA' # Nom de la licence, image dans le répertoire 'img'
LICENCE_PREMIERE = True # Licence sur la première image
LICENCE_TOUTES = False # Ajouter la licence sur toutes les images
AJOUT_DATES = True # Ajouter la date sur toutes les images
AJOUT_NOMS = True
AJOUT_ETALON = True # Ajouter un étalon de distance sur le première image
ETALON_H = True # Ajouter l'étalon horizontalement
VIDEO = True # Générer la vidéo
# Paramètres des astres
ASTRES = {'sun':{'nom' : 'Soleil', 'ratio' : 1.4, 'pos' : 0, 'im' : None},
          'mercury':{'nom' : 'Mercure', 'ratio' : 0.5, 'pos' : 1, 'im' : None},
          'venus':{'nom' : 'Vénus', 'ratio' : 0.95, 'pos' : 2, 'im' : None},
          'earth':{'nom' : 'Terre', 'ratio' : 1.0, 'pos' : 3, 'im' : None},
          'moon':{'nom' : 'Lune', 'ratio' : 0.3, 'pos' : 3.5, 'im' : None},
          'mars':{'nom' : 'Mars', 'ratio' : 0.7, 'pos' : 4, 'im' : None},
          'jupiter':{'nom' : 'Jupiter', 'ratio' : 1.2, 'pos' : 5, 'im' : None},
          'saturn':{'nom' : 'Saturne', 'ratio' : 1.3, 'pos' : 6, 'im' : None},
          'neptune':{'nom' : 'Neptune', 'ratio' : 1.1, 'pos' : 8, 'im' : None},
          'pluto':{'nom' : 'Pluton', 'ratio' : 0.5, 'pos' : 9, 'im' : None}
         }
# -----------------------------------------------------------------------------

# Initialisation des variables globales
DATES = []
coord_km_x = []
coord_km_y = []
ymin = 0
ymax = 0
astres = ['sun'] + PLANETS

# Redimensionnement des tailles des astres
for a in ASTRES:
    im = Image.open('img/{}.png'.format(a), mode='r')
    newsize = int(TAILLE_REF*float(ASTRES[a]['ratio']))
    im = im.resize((newsize, newsize), Image.ANTIALIAS)
    ASTRES[a]['im'] = im

# Calcul des coordonnées des astres, en km, pour chaque date
def get_coordinates(dat, planetes):
    t = Time(str(dat))
    s = get_body_barycentric('sun', t, ephemeris='de430')
    s_ = s-s
    cx = np.array([float(str(s_.x).split()[0])])
    cy = np.array([float(str(s_.y).split()[0])])
    for p in planetes:
        bb = get_body_barycentric(p, t, ephemeris='de430') - s
        xy = [float(str(bb.x).split()[0]), float(str(bb.y).split()[0])]
        cx = np.vstack((cx, np.array(xy[0])))
        cy = np.vstack((cy, np.array(xy[1])))
    return cx, cy
d = DEBUT
delta = timedelta(days=INTERVALLE)
while d <= FIN:
    DATES.append(d.strftime("%Y-%m-%d"))
    c_x, c_y = get_coordinates(d, PLANETS)
    coord_km_x.append(c_x)
    coord_km_y.append(c_y)
    ymin = min(min(c_y), ymin)
    ymax = max(max(c_y), ymax)
    d += delta

# Calcul des coordonnées des astres, en pixel
maxi = max(abs(ymax), abs(ymin))
maxo = 0
for a in astres:
    if ASTRES[a]['pos'] > maxo:
        last_planet = ASTRES[a]
ratio = IMG_P*(IMG_H/2 - last_planet['im'].size[1])/maxi
coord_pix_x = np.vectorize(lambda x: x*ratio+(IMG_L/2))(coord_km_x)
coord_pix_y = np.vectorize(lambda y: y*ratio+(IMG_H/2))(coord_km_y)

# Calcul de l'étalon de distance (distance Soleil-Terre)
def etalon():
    """ Calcule les coordonnées"""
    t = Time(DATES[0])
    s = get_body_barycentric('sun', t, ephemeris='de430')
    e = get_body_barycentric('earth', t, ephemeris='de430') - s
    x = float(str(e.x).split()[0])
    y = float(str(e.y).split()[0])
    return sqrt((x)**2 + (y)**2)

# Production des images
try:
    IMG_FOND = Image.open("img/{}".format(IMAGE_FOND))
    if IMG_FOND.size != (IMG_L, IMG_H):
        IMG_FOND = IMG_FOND.resize((IMG_L, IMG_H), Image.ANTIALIAS)
except IOError:
    IMG_FOND = Image.new('RGB', (IMG_L, IMG_H), color='black')

for i in range(len(coord_km_x)):
    pix_x = coord_pix_x[i]
    pix_y = coord_pix_y[i]
    img = IMG_FOND.copy()
    for num, astre in enumerate(astres):
        im = ASTRES[astre]['im']
        size = im.size
        img.paste(im, (int(pix_x[num])-int(size[0]/2), int(pix_y[num])-int(size[1]/2)), im)
        if i == 0:
            if astres[num] == 'earth':
                earth_x = pix_x[num]
                earth_y = pix_y[num]
            if astres[num] == 'sun':
                sun_x = pix_x[num]
                sun_y = pix_y[num]
            if AJOUT_NOMS:
                fnt = ImageFont.truetype(POLICE, TAILLE_POLICE_NOMS)
                d = ImageDraw.Draw(img)
                #print(ASTRES[astre]['nom'])
                #print(pix_x+(size[0])/2)
                d.text((pix_x[num]+(size[0])/2+5,pix_y[num]), str(ASTRES[astre]['nom']), font=fnt, fill=COULEUR_NOMS)
                
    if (i == 0 and LICENCE_PREMIERE) or (i > 0 and LICENCE_TOUTES):
        lic = Image.open('img/{}.png'.format(LICENCE_TYPE), mode='r')
        img.paste(lic, (MARGE, IMG_H-lic.size[1]-MARGE))
        fnt = ImageFont.truetype(POLICE, TAILLE_POLICE_AUTEUR)
        d = ImageDraw.Draw(img)
        d.text((MARGE+5+lic.size[0], IMG_H-lic.size[1]-MARGE), AUTEUR, font=fnt, fill=COULEUR_AUTEUR)
    if AJOUT_DATES:
        d = ImageDraw.Draw(img)
        fnt = ImageFont.truetype(POLICE, TAILLE_POLICE)
        date = datetime.strptime(str(DATES[i]), '%Y-%m-%d')
        d.text((MARGE,MARGE), "{:%d %B %Y}".format(date), font=fnt, fill=COULEUR_DATE)
    if i == 0 and AJOUT_ETALON:
        d = ImageDraw.Draw(img)
        distance = "Distance S-T : {:.2E} m".format(1000*etalon())
        fnt = ImageFont.truetype(POLICE, TAILLE_POLICE_ETALON)
        d.text((MARGE,MARGE+1.5*TAILLE_POLICE), distance, font=fnt, fill=COULEUR_ETALON)
        if ETALON_H:
            distance_px = sqrt((earth_x-sun_x)**2 + (earth_y-sun_y)**2)
            d.line((MARGE, MARGE+3.5*TAILLE_POLICE, MARGE+distance_px, MARGE+3.5*TAILLE_POLICE), width=2, fill=COULEUR_ETALON)
            d.line((MARGE, MARGE+3.5*TAILLE_POLICE-3, MARGE, MARGE+3.5*TAILLE_POLICE+4), width=2, fill=COULEUR_ETALON)
            d.line((MARGE+distance_px, MARGE+3.5*TAILLE_POLICE-3, MARGE+distance_px, MARGE+3.5*TAILLE_POLICE+4), width=2, fill=COULEUR_ETALON)
        else:
            d.line((sun_x, sun_y, earth_x, earth_y), width=4, fill=COULEUR_ETALON)


        
    img.save('media/img{:02}.png'.format(i))

# Production de la vidéo
if VIDEO:
    CMD = "ffmpeg -y -r 10  -pattern_type glob -i 'media/*.png' -b:v 1200k media/video.avi"
    os.system(CMD)
