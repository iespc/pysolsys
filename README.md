# Python solar system
-----------------------
pysolsys est un simple script Python permettant de représenter les positions de certaines planètes autour du soleil, à différentes dates, et de générer une vidéo de type "chronophotographie".

## Dépdendances
* Python 3 : <https://www.python.org/>
* PIL (Pillow) : <http://python-pillow.org/>
* Astropy : <http://www.astropy.org/>
* pyjplephem : <https://pypi.org/project/jplephem/>
* FFMpeg : <https://www.ffmpeg.org/>

## Licence
Copyright (C)2021 iespc.free.fr
Ce projet est un logiciel libre : vous pouvez le redistribuer, le modifier selon les terme de la GPL (GNU Public License) dans les termes de la Free Software Foundation concernant la version 3 ou plus de la dite licence.
Ce programme est fait avec l'espoir qu'il sera utile mais SANS AUCUNE GARANTIE.
Lisez la licence pour plus de détails. <http://www.gnu.org/licenses/>.
